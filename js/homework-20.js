"use strict";

// Упражнение 1
let arr = [];
for (let i = 1; i < 21; i++) {
  if (i % 2 == 0) {
    arr.push(i);
  }
}
console.log(arr);

// Упражнение 2
let summ = 0;
let num;
for (let n = 0; n < 3; n++) {
  num = Number(prompt(`Введите ${n + 1} число:`, ""));
  if (!Number.isFinite(num)) {
    alert("Ошибка, вы ввели не число");
    break;
  }
  summ += num;
}
alert(`Результат сложения: ${summ}`);

// Упражнение 3
let arrayMonth = [
  "Январь",
  "Февраль",
  "Март",
  "Апрель",
  "Май",
  "Июнь",
  "Июль",
  "Август",
  "Сентябрь",
  "Октябрь",
  "Ноябрь",
  "Декабрь",
];

function getNameOfMonth(value) {
  console.log(value);
  if (!Number.isFinite(value) || (value < 1 && value > 12)) {
    return alert("Неверное значение");
  }

  return alert(arrayMonth[value - 1]);
}
getNameOfMonth(2);

for (let k = 0; k < 12; k++) {
  if (k == 9) continue;
  console.log(arrayMonth[k]);
}

// Упражнение 4
(() => {
  console.log("IIFE function");
  // Функция которая выполняется в момент её объявления.
  // Переменные внутри этой функции не засоряют глобальную область видимости.
})();

"use strict";

// Упражнение 1

let a = "100px";
let b = "323px";
let result = parseInt(a) + parseInt(b);
console.log(result);

// Упражнение 2

let array = [10, -45, 102, 36, 12, 0, -1];
console.log(Math.max(...array));

// Упражнение 3

let c = 0.111;
console.log(Math.ceil(c));

let e = 45.333333;
console.log(+e.toFixed(1));

let f = 3;
console.log(f ** 5);

let g = 400000000000000;
console.log("4e14");

let h = "1" != 1;
console.log((h = "1" !== 1));

// Упражнение 4
console.log(0.1 + 0.2 === 0.3); // Вернёт false, почему?
/*
Из-за не точного представления числа в двоичной системе счисления
(становятся бесконечной дробью).
*/

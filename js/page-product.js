'use strict';

const fbForm = document.querySelector('.form-review'),
  inputName = fbForm.querySelector('.form-review__input-user-name'),
  inputGrade = fbForm.querySelector('.form-review__rating'),
  nameErr = fbForm.querySelector('.form-review__user-name-err'),
  gradeErr = fbForm.querySelector('.form-review__grade-err'),
  inputsForm = fbForm.querySelectorAll('input, textarea');

fbForm.addEventListener('submit', (evt) => {
  evt.preventDefault();
  validUserName();
  validGrade();
  if (nameErr.classList.contains('visibleErr'))
    gradeErr.classList.remove('visibleErr');
  if (
    !nameErr.classList.contains('visibleErr') &&
    !gradeErr.classList.contains('visibleErr')
  ) {
    Object.keys(localStorage).forEach((item) => {
      localStorage.removeItem(item);
    });
  }
});

function validUserName() {
  inputName.value = inputName.value.trim();
  inputName.removeEventListener('focus', onFocus(inputName, nameErr));
  inputName.value.length === 0
    ? action('Вы забыли указать имя и фамилию')
    : initial() ?? inputName.value.length < 2
    ? action('Имя не может быть короче 2-хсимволов')
    : initial();
  function action(message) {
    nameErr.classList.add('visibleErr');
    inputName.classList.add('visibleErr');
    nameErr.textContent = message;
  }
  function initial() {
    nameErr.classList.remove('visibleErr');
    inputName.classList.remove('visibleErr');
  }
  onFocus(inputName, nameErr);
}

function validGrade() {
  inputGrade.value = inputGrade.value.trim();
  inputGrade.removeEventListener('focus', onFocus(inputGrade, gradeErr));
  if (
    inputGrade.value === '' ||
    isNaN(inputGrade.value) ||
    inputGrade.value > 5 ||
    inputGrade.value < 1
  ) {
    gradeErr.classList.add('visibleErr');
    inputGrade.classList.add('visibleErr');
    gradeErr.textContent = 'Оценка должна быть от 1 до 5';
  } else {
    inputGrade.classList.remove('visibleErr');
  }
  onFocus(inputGrade, gradeErr);
}

function onFocus(el, err) {
  el.addEventListener('focus', () => {
    err.classList.remove('visibleErr');
  });
}

// Storage
inputsForm.forEach((el) => {
  el.addEventListener('input', () => {
    localStorage.setItem(el.name, el.value);
  });
  el.value = localStorage.getItem(el.name);
});

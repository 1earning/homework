const listColor = document.querySelector(".colors__list"),
  colorValue = document.querySelector(".colors__value"),
  listMemory = document.querySelector(".memory__list"),
  memoryValue = document.querySelector(".memory__value");

function changeTitle(event, value) {
  if (event.target.tagName === "INPUT") {
    value.textContent = event.target.value;
  }
}

listColor.onchange = function (event) {
  changeTitle(event, colorValue);
};

listMemory.onchange = function (event) {
  changeTitle(event, memoryValue);
};

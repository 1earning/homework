'use strict';

// Упражнение 1
let array = ['null', true, 10, {}, 10, ['abc', 100], 2, 2, 1, 10, 5, false];

function getSumm(arr) {
  let getNumberSum = arr.reduce((sum, item) => {
    if (Number.isFinite(item)) {
      sum += item;
    }

    return sum;
  }, 0);

  return getNumberSum;
}
console.log('Сумма чисел в объекте:', getSumm(array));

// Упражнение 2
// В файле data.js

// Упражнение 3
let cart = [4884, 123, 3332, 5234];
console.log('Начальный массив:', ...cart);

// Добавление в корзину
function addToCart(itemId) {
  if (!cart.includes(itemId)) return cart.push(itemId);
}

addToCart(2232);
addToCart(2222);
addToCart(2222);
console.log('После добавления:', ...cart);

// Удаление из корзины
function removeFromCart(itemId) {
  if (cart.indexOf(itemId) > -1) return cart.splice(cart.indexOf(itemId), 1);
}
// function removeFromCart(itemId) {
//   cart.map((el) => {
//     if (el === itemId) {
//       cart.splice(cart.indexOf(el), 1);
//     }
//   });
//   return cart;
// }
removeFromCart(2232);
console.log('После удаления:', ...cart);

const body = document.body,
  darkCheckbox = document.querySelector('.dark-mode__input');

darkCheckbox.onchange = () => {
  let modeIcon = document.querySelector('.dark-mode__icon');

  body.classList.toggle('dark-theme');
  darkCheckbox.checked
    ? (modeIcon.src = './src/sun.webp')
    : (modeIcon.src = './src/moon.webp');
};

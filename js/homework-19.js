"use strict";

// Упражнение 1
let a = "$100";
let b = "100$";
let summ = Number(a.slice(1)) + Number(b.slice(0, -1));
console.log(summ);

// Решение упражнения 1 через регулярное выражение
let regexp = /\d+/gi;
let sum = Number(a.match(regexp)) + Number(b.match(regexp));
console.log(sum);

// Упражнение 2
let message = "    привет, медвед          ";
message = message.trim();
message = message[0].toUpperCase() + message.slice(1).toLowerCase();
console.log(message);

// Упражнение 3
let age = Number(prompt("Сколько вам лет?", 18));
let msg;
if (0 <= age && age <= 3) {
  msg = "младенец";
} else if (4 <= age && age <= 11) {
  msg = "ребенок";
} else if (12 <= age && age <= 18) {
  msg = "подросток";
} else if (19 <= age && age <= 40) {
  msg = "познаёте жизнь";
} else if (41 <= age && age <= 80) {
  msg = "познали жизнь";
} else if (81 <= age) {
  msg = "долгожитель";
} else {
  msg = "уверены в этом?";
}
alert(`Вам ${age} лет и вы ${msg}`);

// Упражнение 4
let SMS = "Я работаю со строками как профессионал!";
let count = SMS.split(" ");
console.log(`В строке: ${count.length} элементов`);

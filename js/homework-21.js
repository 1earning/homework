"use strict";

// Упражнение 1

/**
 * Проверка объекта на пустоту
 *
 * @param {Object.<string, number>} obj Объект проверки
 * @returns {boolean} Результат проверки
 */
function isEmpty(obj) {
  for (let key in obj) {
    return false;
  }

  return true;
}

let book = {};
console.log(isEmpty(book));

// Упражнение 2
// В файле @data.js

// Упражнение 3

/**
 * Расчёт общей суммы значений свойств объекта с учётом добавленного процента и вывод в новый объект.
 *
 * @param {Object.<number>} object Объект со значениями
 * @param {number} percent Значение процента
 * @returns Новый объект с добавленным процентом.
 */
let sumSalaries = 0;
function raiseSalary(object, percent) {
  let newObject = {};
  for (let key in object) {
    newObject[key] = Math.floor(object[key] + (object[key] / 100) * percent);
    sumSalaries += newObject[key];
  }

  return newObject;
}

let salaries = {
  John: 100000,
  Ann: 160000,
  Pete: 130000,
};
console.log(raiseSalary(salaries, 5));
console.log(sumSalaries);

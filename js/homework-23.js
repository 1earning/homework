'use strict';

// Упражнение 1
function loadTimer() {
  let secondsOfTimer = Number(prompt('Введите число для начала отсчёта:', 5));
  if (!Number.isFinite(secondsOfTimer) || secondsOfTimer < 0) {
    alert('Не верный ввод!');
  } else {
    let timer = setInterval(() => {
      console.log('Осталось:', secondsOfTimer--, 'c');
      if (secondsOfTimer === -1) {
        clearInterval(timer);
        console.log('Время вышло!');
      }
    }, 1000);
  }
}

loadTimer();

// Упражнение 2
function request(url) {
  console.time('delay');
  fetch(url)
    .then((response) => response.json())
    .then((users) => {
      console.log('Получили пользователей:', users.data.length);
      users.data.forEach((user) => {
        console.log(
          '—',
          user.first_name,
          user.last_name,
          '(' + user.email + ')'
        );
      });
    })
    .catch((err) => {
      console.error(`Упс... ` + err);
    });
  console.timeEnd('delay');
}

request('https://reqres.in/api/users');
